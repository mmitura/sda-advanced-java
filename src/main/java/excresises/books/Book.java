package excresises.books;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Book {
    String title;
    int price;
    int yearPublished;
    List<Author> authorList;
    Genre genre;
}
