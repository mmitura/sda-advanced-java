package excresises.books;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static excresises.books.Genre.*;

public class BookService {
    List<Book> bookList = new ArrayList<>();

    void addBook(Book book) {
        bookList.add(book);
    }

    void removeBook(Book book) {
        bookList.remove(book);
    }

    List<Book> getAllBooks() {
        return bookList.stream()
                .collect(Collectors.toList());
    }

    List<Book> getAllFantasyBooks() {
        return bookList.stream()
                .filter(book -> book.getGenre().equals(FANTASY))
                .collect(Collectors.toList());
    }

    List<Book> getBooksPublishedBefore(int year) {
        return bookList.stream()
                .filter(book -> book.getYearPublished() < year)
                .collect(Collectors.toList());
    }

    Book getMostExpensiveBook() {
        return bookList.stream()
                .sorted(Comparator.comparingInt(Book::getPrice).reversed())
                .findFirst()
                .get();
    }

    Book getCheapestBook() {
        return bookList.stream()
                .sorted(Comparator.comparingInt(Book::getPrice))
                .findFirst()
                .get();
    }

    Book getBookWithAtleast3Authors() {
        return bookList.stream()
                .filter(book -> book.getAuthorList().size() >= 3)
                .findAny()
                .orElseThrow(() -> new IllegalStateException("Book with atleast 3 authors not found"));
    }

    void getBooksSortedBy(Comparator<Book> comparator) {
        bookList.stream()
                .sorted(comparator)
                .forEach(System.out::println);
    }

    boolean isPresent(Book book) {
        return bookList.stream()
                .anyMatch(streamBook -> streamBook.equals(book));
//        return bookList.stream()
//                .filter(streamBook -> streamBook.equals(book))
//                .findAny()
//                .isPresent();
    }

    List<Book> getBooksByAuthor(Author author) {
        return bookList.stream()
                .filter(book -> book.getAuthorList().contains(author))
                .collect(Collectors.toList());
    }

    Set<Author> getAuthorsOfBooksAsSet() {
        return bookList.stream()
                .flatMap(book -> book.getAuthorList().stream())
                .collect(Collectors.toSet());
    }

    List<Author> getAuthorsOfBooksAsList() {
        return bookList.stream()
                .flatMap(book -> book.getAuthorList().stream())
                .distinct()
                .collect(Collectors.toList());
    }

    int getTotalPriceOfBooks() {
        return bookList.stream()
                .map(Book::getPrice)
                .reduce(0, (subtotal, element) -> subtotal + element);
    }

    List<String> getKids() {
        List<String> kids = new ArrayList<>()
        for (int i = 0; i < bookList.size(); i++) {
            List<Author> authorList = bookList.get(i).getAuthorList();
            for (int j = 0; j < authorList.size(); j++) {
                kids.addAll(authorList.get(j).getKids());
            }
        }

        return bookList.stream()
                .flatMap(book -> book.getAuthorList().stream())
                .flatMap(author -> author.getKids().stream())
                .collect(Collectors.toList());
    }



}

class BookDemo {
    public static void main(String[] args) {
        Author author1 = new Author("John", "Smith", 'M', Arrays.asList("Mark", "Helen"));
        Author author2 = new Author("Jessica", "Albana", 'F', Arrays.asList());
        Author author3 = new Author("Roger", "Moore", 'M', Arrays.asList("Thomas", "Ann"));
        Author author4 = new Author("Catherin", "Nadie", 'F', Arrays.asList("Jessica", "Brian"));
        Book book1 = new Book("Book 1", 34, 2000,
                Arrays.asList(author1), FANTASY);
        Book book2 = new Book("Book 2", 56, 1999,
                Arrays.asList(author2, author3, author4), ACTION);

        BookService bookService = new BookService();
        bookService.addBook(book1);
        bookService.addBook(book2);

//        bookService.getBooksSortedBy(Comparator.comparingInt(Book::getYearPublished));
//        System.out.println(bookService.getBookWithAtleast3Authors());
        bookService.getKids().stream().forEach(System.out::println);
    }
}
