package excresises.books;

public enum Genre {
    FANTASY, ACTION, CRIME
}
