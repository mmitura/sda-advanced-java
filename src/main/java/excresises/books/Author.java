package excresises.books;

import lombok.*;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Author {
    String name, surname;
    char sex;
    List<String> kids;
}
