package excresises;

import java.util.Arrays;
import java.util.function.Predicate;

public class PartOf<T> {

    String partOf(T[] array, Predicate<T> predicate) {
        double length = array.length;
        double count = Arrays.stream(array)
                .filter(predicate)
                .count();
        return (count / length) * 100 + "%";
    }
}
