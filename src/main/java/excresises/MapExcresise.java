package excresises;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapExcresise {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("Java", 12);
        map.put("Javascript", 59);
        map.put("PHP", 21);
        System.out.println(elegantSolution(map));
    }

    static String sloppySolution(Map<String, Integer> map) {
        List<String> list = new ArrayList<>();
        map.forEach((k, v) -> {
            list.add("Klucz: " + k + ", Wartosc: " + v);
        });
        return list.stream()
                .collect(Collectors.joining(",\n")) + ".";
    }

    static String elegantSolution(Map<String, Integer> map) {
        return map.entrySet()
                .stream()
                .map(entry -> "Klucz: " + entry.getKey() + ", Wartosc: " + entry.getValue())
                .collect(Collectors.joining(",\n")) + ".";
    }
}
