package excresises.movie;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@Builder
public class Movie {
    String title;
    int year;
    String director;

}

class MovieDemo {
    public static void main(String[] args) {
        Movie movie1 = Movie.builder()
                .title("Dawno temu w ameryce")
                .year(1995)
                .director("Clint")
                .build();

        Movie movie2 = new Movie("Asd", 1888, "Alan");
        System.out.println(movie1.toString());
        System.out.println(movie2.toString());

    }
}