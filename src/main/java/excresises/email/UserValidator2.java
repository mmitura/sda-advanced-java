package excresises.email;

import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public class UserValidator2 {
    public List<String> validateEmails(String email, String alternativeEmail) {

        @AllArgsConstructor
        class Email2 {
            String email;
            private final static String UNKNOWN = "unknown";
            private final Pattern VALID_EMAIL_ADDRESS_REGEX =
                    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

            String validate() {
                if (Objects.isNull(email) || "".equals(email)) {
                    return UNKNOWN;
                }
                if (VALID_EMAIL_ADDRESS_REGEX.matcher(email).find()) {
                    return email;
                }
                return UNKNOWN;
            }
        }

        return Arrays.asList(new Email2(email).validate(),
                new Email2(alternativeEmail).validate());
    }
}

class Email2Demo {
    public static void main(String[] args) {
        UserValidator2 userValidator2 = new UserValidator2();
        List<String> emailsOk = userValidator2.validateEmails("asdasd@asd.pl", "donald@trump.com");
        List<String> emailsNotOk = userValidator2.validateEmails("asdasdasd.pl", "");
        System.out.println("Okay emails:");
        emailsOk.stream().forEach(System.out::println);

        System.out.println("Wrong emails:");
        emailsNotOk.stream().forEach(System.out::println);
    }
}

