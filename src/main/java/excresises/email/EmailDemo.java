package excresises.email;

import java.util.List;

public class EmailDemo {
    public static void main(String[] args) {
        UserValidator2 userValidator2 = new UserValidator2();
        List<String> emailsOk = userValidator2.validateEmails("asdasd@asd.pl", "donald@trump.com");
        List<String> emailsNotOk = userValidator2.validateEmails("asdasdasd.pl", "");
        System.out.println("Okay emails:");
        emailsOk.stream().forEach(System.out::println);

        System.out.println("Wrong emails:");
        emailsNotOk.stream().forEach(System.out::println);
    }
}
