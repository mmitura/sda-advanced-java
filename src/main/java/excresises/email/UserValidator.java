package excresises.email;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
@Getter
public class UserValidator {
    @Override
    public String toString() {
        return "UserValidator{" +
                "email='" + email + '\'' +
                ", emailList=" + emailList +
                '}';
    }

    private String email;
    private List<Email> emailList = new ArrayList<>();
    public void validateEmails(String email, String alternativeEmail){
        Email email1 = new Email(email);
        Email email2 = new Email(alternativeEmail);
        emailList.add(email1);
        emailList.add(email2);
    }
}
class Email {
    String email;
    @Override
    public String toString() {
        return "email=" + email + " : ";
    }
    public Email(String email) {
        if(email.isEmpty() || !checkEmail(email)){
            this.email = "Unkown";
        } else{
            this.email = email;
        }
    }
    public boolean checkEmail (String email){
        if(email.contains("@") && email.contains(".")){
            return true;
        }
        return false;
    }
}

class Demo2 {
    public static void main(String[] args) {
        UserValidator userValidator = new UserValidator();
        userValidator.validateEmails("gkkfkkd@wp.pl", "sdfasdfasdf");
        UserValidator userValidator1 = new UserValidator();
        userValidator1.validateEmails("dfsdfsads@o2.pl", "sdfasdfasd@f");
        System.out.println(userValidator.getEmailList());
        System.out.println(userValidator1.getEmailList());
    }
}