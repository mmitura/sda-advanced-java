package excresises;

import java.util.function.Predicate;

public class PartOfDemo {

    public static void main(String[] args) {

        Integer[] integers = new Integer[]{1, 2, 3, 5, 8, 29, 232, 2323};
        Predicate<Integer> greaterThanFive = i -> i > 5;
        PartOf<Integer> integerPartOf = new PartOf<>();
        System.out.println(integerPartOf.partOf(integers, i -> i > 5));

        String[] strings = new String[]{"Kasia", "Basia", "Stasia"};
        Predicate<String> startsWithK = s -> s.substring(0,1).equalsIgnoreCase("k");
        PartOf<String> stringPartOf = new PartOf<>();
        System.out.println(stringPartOf.partOf(strings, startsWithK));


    }
}
