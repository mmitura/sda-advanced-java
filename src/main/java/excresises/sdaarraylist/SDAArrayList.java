package excresises.sdaarraylist;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Arrays;
import java.util.Objects;

public class SDAArrayList<T> {
    Object[] array;

    void add (T element) {
        if (Objects.isNull(array)) {
            array = new Object[]{element};
            return;
        }

        Object[] newArray = new Object[array.length+1];
        for(int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        newArray[array.length] = element;
        array = newArray;
    }

    void remove(int index) {
        Object[] newArray = new Object[array.length-1];
        for(int i = 0; i< newArray.length; i++) {
            if (i < index) {
                newArray[i] = array[i];
            } else {
                newArray[i] = array[i+1];
            }
        }
        array = newArray;
    }

    T get(int index) {
        return (T) array[index];
    }

    void display() {
        Arrays.stream(array)
                .forEach(element -> System.out.println(element.toString()));
    }
}

class ListDemo {
    public static void main(String[] args) {
        SDAArrayList<String> stringSDAArrayList = new SDAArrayList<>();
        stringSDAArrayList.add("asd0");
        stringSDAArrayList.add("asd1");
        stringSDAArrayList.add("asd2");
        stringSDAArrayList.add("asd3");
        stringSDAArrayList.add("asd4");

        stringSDAArrayList.remove(3);

        System.out.println(stringSDAArrayList.get(3));
    }
}
