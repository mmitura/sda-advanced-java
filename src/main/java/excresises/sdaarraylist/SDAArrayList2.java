package excresises.sdaarraylist;

import java.util.Arrays;
import java.util.Objects;

import static javax.management.Query.in;
import static javax.management.Query.not;

public class SDAArrayList2<T> {
    Object[] array = new Object[0];

    void add (T element) {
        int length = array.length;
        array = Arrays.copyOf(array, length + 1);
        array[length] = element;
    }



    void display() {
        Arrays.stream(array)
                .forEach(element -> System.out.println(element.toString()));
    }

    public void remove(int index) {
        if (index < 0 || index > array.length) {
            throw new IllegalStateException("Index out of bounds");
        }
        Object[] newArray = new Object[array.length - 1];
        for (int i = 0; i<array.length; i++) {
            if (i < index) {
                newArray[i] = array[i];
            } else if (i > index) {
                newArray[i-1] = array[i];
            }
        }
        array = newArray;
    }

    T get(int index){
        return (T) array[index];
    }

    void removeAll(T element) {
        array = Arrays.stream(array)
                .filter(elem -> !elem.equals(element))
                .toArray();
//        for(int i = 0; i<array.length; i++) {
//            if (array[i].equals(element)) {
//                remove(i);
//            }
//        }
    }

}

class ListDemo2 {
    public static void main(String[] args) {
        SDAArrayList2<String> stringSDAArrayList2 = new SDAArrayList2<>();
        stringSDAArrayList2.add("string0");
        stringSDAArrayList2.add("string1");
        stringSDAArrayList2.add("string2");
        stringSDAArrayList2.display();

        SDAArrayList2<Integer> integerSDAArrayList2 = new SDAArrayList2<>();
        integerSDAArrayList2.add(1);
        integerSDAArrayList2.add(24);
        integerSDAArrayList2.add(24);
        integerSDAArrayList2.add(24);
        integerSDAArrayList2.add(24);
        integerSDAArrayList2.add(55);
        integerSDAArrayList2.removeAll(24);
        integerSDAArrayList2.display();

    }
}
