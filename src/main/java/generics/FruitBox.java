package generics;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Setter
@AllArgsConstructor
public class FruitBox<T extends Fruit> {
    private T fruit;

    public T getFruit() {
        return fruit;
    }
}
