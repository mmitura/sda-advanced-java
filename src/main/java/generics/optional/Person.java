package generics.optional;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString

@AllArgsConstructor
public class Person {
    String name;
    int age;

    public Person setAge(int age) {
        this.age = age;
        return this;
    }


}
