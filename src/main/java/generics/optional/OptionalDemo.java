package generics.optional;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

public class OptionalDemo {

    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Monika", 22));
        personList.add(new Person("Tomek", 40));
        personList.add(new Person("Tomek", 220));

        personList.stream()
                .filter(person -> person.getAge() <= 18)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("There's no person younger than 18!"));

        for (int i = 0; i <= personList.size(); i++) {
            if (personList.get(i).getAge() <= 18) {
                System.out.println(personList.get(i));
            }
        }

//        System.out.println(person1);

    }
}
