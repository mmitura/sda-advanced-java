package generics;

import lombok.ToString;

@ToString
public class Orange implements Fruit {
    @Override
    public void info() {
        System.out.println("I'm an orange!");
    }
}
