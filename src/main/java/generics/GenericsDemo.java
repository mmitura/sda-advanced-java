package generics;

public class GenericsDemo {
    public static void main(String[] args) {
        AppleBox appleBox = new AppleBox();
        appleBox.setApple(new Apple());

//        appleBox.getApple().info();

        FruitBox<Fruit> fruitBox = new FruitBox<>(new Orange());
//        FruitBox<Carrot> tFruitBox = new FruitBox<Carrot>(new Carrot());
//        tFruitBox.getFruit().info();




    }
}
