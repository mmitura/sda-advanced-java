package generics;

import lombok.ToString;

@ToString
public class Apple implements Fruit {

    @Override
    public void info() {
        System.out.println("I'm an apple!");
    }
}
