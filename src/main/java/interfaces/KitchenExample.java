package interfaces;

import java.util.ArrayList;
import java.util.List;

public class KitchenExample {
    public static void main(String[] args) {
        List<AGD> agdList = new ArrayList<AGD>();
        agdList.add(new Fridge());
        agdList.add(new Microwave());
        AGD agd = new Fridge();
        agdList.forEach(AGD::turnOn);


    }

    public void doSmth(AGD agd) {

    }
}
