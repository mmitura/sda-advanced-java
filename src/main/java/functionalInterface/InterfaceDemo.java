package functionalInterface;

import generics.optional.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class InterfaceDemo {
    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Monika", 22));
        personList.add(new Person("Tomek", 40));
        personList.add(new Person("Grzes", 10));

        // Na wejsciu operujemy na liscie, ktora przechozdi w stream
        // operacja map() przyjmuje inter. funkcyjny Function, w naszym przypadku
        // przyjmuje on Person a zwraca String (nastepuje przemapowanie obiektow Person->String)
        // operacja filter() przyjmuje Predykat, który przyjmuje String a zwraca zawsze boolean
        // obiekty, gdzie predykat zwrócił false zostają usuniete ze streama
        // operacja forEach() przyjmuje nasz stream Stringów, wykonuje operacje Sout
        // jednak nic nie zwraca (void) kończąc tym samym życie streama

        personList
                .stream()
                .map(person -> person.getName())
                .filter(InterfaceDemo::nameLengthShorterThan6)
                .forEach(name -> System.out.println(name));




    }

    private static boolean nameLengthShorterThan6(String s) {
        return s.length() < 6;
    }
}
