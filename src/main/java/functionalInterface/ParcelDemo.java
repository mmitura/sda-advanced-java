package functionalInterface;

public class ParcelDemo {
    public static void main(String[] args) {
        Parcel parcel = new Parcel(33, 35, 38, 12F, true);
        System.out.println(Validator.validate(parcel));
    }
}
