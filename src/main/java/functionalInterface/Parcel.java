package functionalInterface;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class Parcel {
    int x, y, z;
    float weight;
    boolean express;
}
