package functionalInterface;

import java.util.function.Predicate;

public interface Validator {
    static boolean validate(Parcel parcel) {
        reportErrors(parcel);
        return isNotTooLarge()
                .and(isNotTooHeavy())
                .and(noSideIsTooSmall)
                .test(parcel);
    }

    static void reportErrors(Parcel parcel) {
        if (isNotTooLarge().negate().test(parcel)) {
            System.out.println("Parcel is too large");
        }
        if (isNotTooHeavy().negate().test(parcel)) {
            System.out.println("Parcel is too heavy");
        }
        if (noSideIsTooSmall.negate().test(parcel)) {
            System.out.println("Parcel has too small sides");
        }
    }

//    static Predicate<? super Parcel> noSideIsTooSmall() {
//        return parcel -> parcel.getX() > 30
//                && parcel.getY() > 30
//                && parcel.getZ() > 30;
//    }
    Predicate<Parcel> noSideIsTooSmall = parcel -> parcel.getX() > 30
                                                && parcel.getY() > 30
                                                && parcel.getZ() > 30;

    static Predicate<Parcel> isNotTooLarge() {
        return parcel -> parcel.getX() + parcel.getY() + parcel.getZ() < 300;
    }

    static Predicate<Parcel> isNotTooHeavy() {
        return parcel -> {
            if (parcel.isExpress()) {
                return parcel.getWeight() < 15F;
            } else {
                return parcel.getWeight() < 30F;
            }
        };
    }


}
